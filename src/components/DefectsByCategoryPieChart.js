import React from 'react';
import styled from 'styled-components';
import { PieChart, Pie, Legend, Tooltip, Cell } from 'recharts';

const COLORS = ['#00acc1', '#4285f4', '#db4437', '#f4b400', '#0f9d58', '#ab47bc'];

const data = [
  {name: 'In-Flight Entertainment', value: 12},
  {name: 'Lavatory', value: 8},
  {name: 'Seat', value: 24},
  {name: 'Overhead Compartment', value: 14},
  {name: 'Lifejacket', value: 7},
  {name: 'Galley', value: 38},
];

const DefectsByCategoryPieChartContainer = styled.div`
  background: #deebf1;
  margin: 10px;
`;

const Header = styled.p`
  font-size: 12px;
  text-transform: uppercase;
  margin: 14px 0 4px 14px;
  text-align: left;
`;

const DefectsByCategoryPieChart = () => (
  <DefectsByCategoryPieChartContainer>
    <Header>Defects by Category - June</Header>
    <PieChart width={300} height={120}>
      <Pie data={data} innerRadius={24} outerRadius={48} fill="#82ca9d">
        {
          data.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>)
        }
      </Pie>
      <text x={150} y={60} textAnchor="middle" dominantBaseline="middle">
        103
      </text>
      <Tooltip/>
  </PieChart>
 </DefectsByCategoryPieChartContainer>
);

export default DefectsByCategoryPieChart;