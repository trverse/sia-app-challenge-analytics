import React from 'react';
import styled from 'styled-components';

import DefectsByStatusBarGraph from './DefectsByStatusBarGraph';
import DefectTurnaroundTimeLineGraph from './DefectTurnaroundTimeLineGraph';
import DefectsByCategoryPieChart from './DefectsByCategoryPieChart';
import DefectsTrend from './DefectsTrend';
import CostByDefectPieChart from './CostByDefectPieChart';
import OverviewNumbers from './OverviewNumbers';

const DashboardContainer = styled.div`
  flex: 0 1 auto;
  height: 768px;
  width: 1024px;
  background: #a6d1e3;
  margin: 0 auto;
`;

const Row = styled.div`
  display: flex;
  flex: 0 1 auto;
  flex-direction: row;
  justify-content: space-between; 
`;

const Column = styled.div`
  display: flex;
  flex-direction: column;
`

const Dashboard = () => (
  <DashboardContainer>
    <Row>
      <OverviewNumbers
        header={'Total Defects'}
        text={1023}
      />
      <OverviewNumbers
        header={'Total Cost'}
        text={'$12077.67'}
      />
      <OverviewNumbers
        header={'Unresolved'}
        text={109}
      />
      <OverviewNumbers
        header={'Resolved'}
        text={914}
      />
    </Row>
    <Row>
      <DefectsTrend />
      <DefectsByStatusBarGraph />
    </Row>
    <Row>
      <DefectTurnaroundTimeLineGraph />
      <Column>
        <DefectsByCategoryPieChart />
        <CostByDefectPieChart />
      </Column>
    </Row>
  </DashboardContainer>
);

export default Dashboard;
