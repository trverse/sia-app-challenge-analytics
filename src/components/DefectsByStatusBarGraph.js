import React from 'react';
import styled from 'styled-components';

import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';

const data = [
  { name: 'Jan', unresolved: 12, resolved: 35 },
  { name: 'Feb', unresolved: 8, resolved: 17 },
  { name: 'Mar', unresolved: 15, resolved: 39 },
  { name: 'Apr', unresolved: 14, resolved: 43 },
  { name: 'May', unresolved: 9, resolved: 34 },
  { name: 'Jun', unresolved: 16, resolved: 44 },
  { name: 'Jul', unresolved: 14, resolved: 35 },
  { name: 'Aug', unresolved: 15, resolved: 28 },
  { name: 'Sep', unresolved: 15, resolved: 29 },
  { name: 'Oct', unresolved: 9, resolved: 43 },
  { name: 'Nov', unresolved: 7, resolved: 35 },
  { name: 'Dec', unresolved: 5, resolved: 33 },
];

const DefectsByStatusBarGraphContainer = styled.div`
  background: #deebf1;
  margin: 10px;
`;

const Header = styled.p`
  font-size: 12px;
  text-transform: uppercase;
  margin: 14px 0 16px 14px;
  text-align: left;
`;

const DefectsByStatusBarGraph = () => (
  <DefectsByStatusBarGraphContainer>
    <Header>Defects by Status</Header>
    <BarChart width={490} height={240} data={data}
      margin={{top: 5, right: 30, left: 20, bottom: 5}}>
      <XAxis dataKey="name" name="month"/>
      <YAxis/>
      <CartesianGrid strokeDasharray="3 3"/>
      <Tooltip/>
      <Legend />
      <Bar name="Unresolved" dataKey="unresolved" stackId="a" fill="#00CDFF" />
      <Bar name="Resolved" dataKey="resolved" stackId="a" fill="#FFA96A" />
    </BarChart>
  </DefectsByStatusBarGraphContainer>
);

export default DefectsByStatusBarGraph;
