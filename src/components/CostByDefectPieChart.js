import React from 'react';
import styled from 'styled-components';
import { PieChart, Pie, Legend, Tooltip, Cell } from 'recharts';

const COLORS = ['#00acc1', '#4285f4', '#db4437', '#f4b400', '#0f9d58', '#ab47bc'];

const data = [
  {name: 'In-Flight Entertainment', value: 53},
  {name: 'Lavatory', value: 36},
  {name: 'Seat', value: 12},
  {name: 'Overhead Compartment', value: 23},
  {name: 'Lifejacket', value: 9},
  {name: 'Galley', value: 27},
];

const Header = styled.p`
  font-size: 12px;
  text-transform: uppercase;
  margin: 14px 0 4px 14px;
  text-align: left;
`;

const CostByDefectPieChartContainer = styled.div`
  background: #deebf1;
  margin: 10px;
  width: 300px;
`;

const CostByDefectPieChart = () => (
  <CostByDefectPieChartContainer>
    <Header>Cost by Defects</Header>
    <PieChart width={300} height={120}>
      <Pie data={data} innerRadius={24} outerRadius={48} fill="#82ca9d">
        {
          data.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>)
        }
      </Pie>
      <text x={150} y={60} textAnchor="middle" dominantBaseline="middle">
        160
      </text>
      <Tooltip/>
  </PieChart>
 </CostByDefectPieChartContainer>
);

export default CostByDefectPieChart;