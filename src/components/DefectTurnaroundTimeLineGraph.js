import React from 'react';
import styled from 'styled-components';
import {ResponsiveContainer, LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';

const data = [
  { name: 'Jan', mins: 24, },
  { name: 'Feb', mins: 36 },
  { name: 'Mar', mins: 27 },
  { name: 'Apr', mins: 38 },
  { name: 'May', mins: 12 },
  { name: 'Jun', mins: 23 },
  { name: 'Jul', mins: 24 },
  { name: 'Aug', mins: 32 },
  { name: 'Sep', mins: 17 },
  { name: 'Oct', mins: 18 },
  { name: 'Nov', mins: 0 },
  { name: 'Dec', mins: 0 },
];

const DefectTurnaroundTimeLineGraphContainer = styled.div`
  background: #deebf1;
  margin: 10px;
`;

const Header = styled.p`
  font-size: 12px;
  text-transform: uppercase;
  margin: 14px 0 16px 14px;
  text-align: left;
`;

const DefectTurnaroundTimeLineGraph = () => (
  <DefectTurnaroundTimeLineGraphContainer>
    <Header>Average Turnaround Time by Defect</Header>
    <LineChart width={680} height={280} data={data}
      margin={{top: 5, right: 30, left: 20, bottom: 5}}>
      <XAxis dataKey="name" name="Month"/>
      <YAxis name="Time"/>
      <CartesianGrid strokeDasharray="3 3"/>
      <Tooltip/>
      <Legend />
      <Line type="monotone" dataKey="mins" name="minutes" stroke="#e52424" activeDot={{r: 5}}/>
    </LineChart>
  </DefectTurnaroundTimeLineGraphContainer>
);

export default DefectTurnaroundTimeLineGraph;
