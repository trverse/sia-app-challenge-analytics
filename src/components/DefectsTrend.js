import React from 'react';
import styled from 'styled-components';

import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';

const data = [
  { name: 'Jan', inflightEnt: 24, seats: 35, lavatory: 49, lifejacket: 3, galley: 31, overheadCompartment: 8 },
  { name: 'Feb', inflightEnt: 36, seats: 42, lavatory: 53, lifejacket: 0, galley: 28, overheadCompartment: 7 },
  { name: 'Mar', inflightEnt: 12, seats: 23, lavatory: 44, lifejacket: 0, galley: 23, overheadCompartment: 7 },
  { name: 'Apr', inflightEnt: 14, seats: 33, lavatory: 43, lifejacket: 2, galley: 34, overheadCompartment: 9 },
  { name: 'May', inflightEnt: 18, seats: 32, lavatory: 35, lifejacket: 0, galley: 20, overheadCompartment: 4 },
  { name: 'Jun', inflightEnt: 22, seats: 28, lavatory: 45, lifejacket: 0, galley: 18, overheadCompartment: 6 },
  { name: 'Jul', inflightEnt: 17, seats: 35, lavatory: 28, lifejacket: 0, galley: 13, overheadCompartment: 4 },
  { name: 'Aug', inflightEnt: 18, seats: 27, lavatory: 14, lifejacket: 0, galley: 39, overheadCompartment: 5 },
  { name: 'Sep', inflightEnt: 24, seats: 43, lavatory: 24, lifejacket: 1, galley: 28, overheadCompartment: 3 },
  { name: 'Oct', inflightEnt: 23, seats: 33, lavatory: 5, lifejacket: 0, galley: 25, overheadCompartment: 7 },
  { name: 'Nov', inflightEnt: 0, seats: 0, lavatory: 14, lifejacket: 0, galley: 0, overheadCompartment: 0 },
  { name: 'Dec', inflightEnt: 0, seats: 0, lavatory: 11, lifejacket: 0, galley: 0, overheadCompartment: 0 },
];

const DefectsTrendContainer = styled.div`
  background: #deebf1;
  margin: 10px;
`;

const Header = styled.p`
  font-size: 12px;
  text-transform: uppercase;
  margin: 14px 0 16px 14px;
  text-align: left;
`;

const DefectsTrend = () => (
  <DefectsTrendContainer>
  <Header>Defects Trend</Header>
  <BarChart width={490} height={240} data={data}
    margin={{top: 20, right: 30, left: 20, bottom: 5}}>
    <XAxis dataKey="name" name="month"/>
    <YAxis/>
    <CartesianGrid strokeDasharray="3 3"/>
    <Tooltip/>
    <Legend />
    <Bar dataKey="inflightEnt" name="In-Flight Entertainment" stackId="a" fill="#88adea" />
    <Bar dataKey="seats" name="Seats" stackId="a" fill="#14369b" />
    <Bar dataKey="lavatory" name="Lavatory" stackId="a" fill="#e0cb41" />
    <Bar dataKey="lifejacket" name="Lifejacket" stackId="a" fill="#ef3a1a" />
    <Bar dataKey="galley" name="Galley" stackId="a" fill="#9858d8" />
    <Bar dataKey="overheadCompartment" name="Overhead Compartment" stackId="a" fill="#53dcf4" />
  </BarChart>
  </DefectsTrendContainer>
);

export default DefectsTrend;
