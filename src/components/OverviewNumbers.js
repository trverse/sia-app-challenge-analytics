import React from 'react';
import styled from 'styled-components';

const OverviewNumbersContainer = styled.div`
  background: #deebf1;
  margin: 10px;
  display: flex;
  flex-direction: column;
  width: 200px;
`;

const Header = styled.p`
  font-size: 12px;
  text-transform: uppercase;
  margin: 14px 0 8px 14px;
  text-align: left;
`;

const Text = styled.p`
  font-size: 26px;
  font-weight: 700;
  margin: 10px 12px;
  text-align: left;
`;

const OverviewNumbers = ({ header = 'Header', text = 'text' }) => (
  <OverviewNumbersContainer>
    <Header>{header}</Header>
    <Text>{text}</Text>
  </OverviewNumbersContainer>
);

export default OverviewNumbers;
